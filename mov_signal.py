import pandas as pd
from optparse import OptionParser


def mov(df, window, pct, code):
    signal_str = f'mov_signal_{window}_{pct}'
    df['pre_close'] = df['close'].shift(1)
    df['high_amp'] = abs(df['high'] / df['pre_close'] - 1)
    df['low_amp'] = abs(df['low'] / df['pre_close'] - 1)
    df['amp'] = df.apply(lambda x: max(x['high_amp'], x['low_amp']), axis=1)
    df = df.dropna()
    df[signal_str] = df[['amp']].rolling(window=window).quantile(pct/100)
    df = df.dropna()
    df = df[[signal_str]]
    df['date_str'] = df.index.strftime("%Y-%m-%d")
    df.to_pickle(f"./data/{stock}/mov_signal/{signal_str}.pkl")


if __name__ == "__main__":
    parser = OptionParser()
    parser.add_option("-s", dest="stock", action="store", type="string")
    (option, args) = parser.parse_args()
    stock = option.stock
    frame = pd.read_csv(f"./data/{stock}_daily.csv", index_col=0, parse_dates=True)

    mov(frame, 20, 90, f"{stock}")
    mov(frame, 20, 95, f"{stock}")
    mov(frame, 30, 90, f"{stock}")
    mov(frame, 30, 95, f"{stock}")
    mov(frame, 40, 90, f"{stock}")
    mov(frame, 40, 95, f"{stock}")
    mov(frame, 60, 90, f"{stock}")
    mov(frame, 60, 95, f"{stock}")
    mov(frame, 180, 90, f"{stock}")
    mov(frame, 180, 95, f"{stock}")
