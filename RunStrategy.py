from auto_call_strategy import AutoCallRelativeGammaHedge, MOVHedgeStrategy
from back_tester import BackTester
from utils.book import Book
from datetime import datetime
from utils.instruments import AutoCallInstrument
from utils.ledger import ProductLedger
from utils.market_data_reader import MarketDataReader

if __name__ == "__main__":
    data = MarketDataReader.load_market_data("600048", datetime(2021, 5, 10), datetime(2021, 8, 10))
    strategy1 = MOVHedgeStrategy(10, 50)
    book = Book.init_book(
        ProductLedger("", AutoCallInstrument("spot", data), 1)
    )
    result = BackTester(book).run(strategy1)
