import copy
from utils.strategy import *


class BackTester(object):
    def __init__(self, book: Book) -> None:
        self.book = book
        self.strategies = []
        self.result = {}

    def run(self, strategy: Strategy):
        print("Starting to run backtest " + strategy.__str__())
        book_ = copy.deepcopy(self.book)
        result_frame = book_.md_frame()
        result_frame[[strategy.tag_pv(), strategy.tag_delta(), strategy.tag_gamma(), strategy.tag_hedge_position(),
                      strategy.tag_hedge_pnl(), strategy.tag_hedge_avg_price()]] =\
            result_frame.apply(lambda x: strategy.signal_run(book_,
                                                              x.name,
                                                              x['is_eod']
                                                            ).evaluate(x.name), axis=1, result_type='expand')

        # result_frame[[strategy.tag_pv(), strategy.tag_delta(), strategy.tag_gamma()]] = result_frame.apply(
        #    lambda x: x['book'].evaluate(x.name), axis=1, result_type='expand')
        # result_frame = result_frame.fillna(method='backfill', axis=1)
        return result_frame.fillna(method='pad', axis=0)

    def summary(self, strategy):
        pass
