#bin/sh
stock=$1

cd data && mkdir $stock
# shellcheck disable=SC2164
cd $stock
mkdir greeks
mkdir spot_new
mkdir mov_signal
cd ../../

python3.7 data_processing.py -s $stock
python3.7 mov_signal.py -s $stock
python3.7 split_data.py -s $stock