from optparse import OptionParser

from auto_call_strategy import *
from back_tester import BackTester
from utils.book import Book
from datetime import datetime
from utils.instruments import AutoCallInstrument
from utils.ledger import ProductLedger
from utils.market_data_reader import MarketDataReader
from matplotlib import pyplot as plt
import pandas as pd
import numpy as np


if __name__ == "__main__":
    parser = OptionParser()
    parser.add_option("-s", dest="stock", action="store", type="string")
    (option, args) = parser.parse_args()
    stock = option.stock

    data = MarketDataReader.load_market_data(stock, datetime(2021, 5, 10), datetime(2021, 8, 10))
    book = Book.init_book(ProductLedger("", AutoCallInstrument(stock, data), 1))

    result_dict = {}

    benchmark = BackTester(book).run(AutoCallEODHedge())
    result_dict['bench_mark'] = benchmark
    days = [20, 30, 40, 60, 180]
    for day in days:
        strategy = MOVHedgeStrategy(day, 95, stock)
        result = BackTester(book).run(strategy)
        result_dict[f"{day}_95"] = result
    print(result_dict)
