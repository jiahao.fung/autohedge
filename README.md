## Infra Doc



Project **Auto Hedge**:

1.  utils

   主要存放数据loader、基本数据结构的定义脚本

   - market_data_reader.py

     - MarketDataReader类：提供与一个静态方法，*load_market_data()* 从文件中读取数据（对于auto call产品，分别读取greeks和spot对应的行情数据）返回一个MarketDataContainer的实例

     - MarketDataContainer类：拿到数据之后，MarketDataContainer对于数据进行处理

       - 1. 增加 *is_eod* 字段，作为strategy一个必要参数，在日末平仓
         2. 对于bump后的(price, pv/greeks)做线性插值
         3. 根据每分钟的close price(set by default，也可改为open, high, low)计算当前对应的pv和greeks
       - spot_price(time)
       - delta(time)
       - gamma(time)
       - delta_pct(time)
       - pv(time)

       属性类的方法分别返回当前时间的市场数据

   - instruments.py : 

     - Instrument抽象类：定义了delta(), gamma(), pv(), dollar_delta()四个抽象方法和可变参数接口
     - AutoCallInstrument类：AutoCall型产品，抽象方法的实现，直接从MarketDataContainer里读取当前时间对应的数据（无需重新计算）
     - Spot类：spot型产品，pv，和dollar delta返回当前价格，delta返回1，gamma返回0
     - EuropeanOption类：返回BlackScholes的解析解（暂未实现gamma）

   - ledger.py: 

     - Ledger抽象类：回测系统中的子单元，实现账本和核算的功能（pv, gamma, dollar_delta的抽象接口）
       - ProductLedger：记录产品（Instrument子类）和当前仓位(数值，如果持有一个AutoCall型产品，则为1)
       - HedgeLedger： 记录对冲账户的对冲*金额*和*持仓（股数）*

   - book.py

     - book类：回测单元，包含一个ProductLedger和一个HedgeLedger，实现时间轮动（以时间为主要变量做两个Ledger的核算）和事件驱动（strategy部分）

   - strategy抽象类：实现不同的标签，和一个signal_gen的抽象接口，该接口会在轮动的时候判断是否需要信号，并完成对冲（调用book的delta_hedge方法），更新完之后返回book

     
    - AutoCallStrategy(Strategy)类，实现固定barrier（param）的auto hedge
    
    
    
~~~python
           def signal_run(self, book: Book, time, is_eod) -> Book:
               md = book.md()
               net_delta = book.delta(time)
               #判断条件：如果是eod或当前账户delta大于threshold，就进行对冲
               if is_eod or abs(net_delta) > self.threshold: 
                   # print(f"Signal for buying {-1 * net_delta} spot at time {time}")
                   hedge_signal = Signal(Spot(md.spot_price(time), md.underlying), -1 * net_delta)
                   book.delta_hedge(hedge_signal, time)
               return book
~~~
       

     - AutoCallRelativeHedgeStrategy(Strategy): 实现spot price对于基准变动一定数值的对冲

       - 具体实现： 初始化的时候实现delta中性，并记录初始价格为基准价格
       - 当现货价格偏离基准价格*一定数值（param）*之后，进行delta对冲，并更新基准价格


~~~python
           def signal_run(self, book: Book, time, is_eod) -> Book:
               md = book.md()
            		#判断条件：如果是eod，或者初始化，或者spot price对于基准价格变动已经超过阈值，进行对冲，并更新基准价格
               if self.benchmark == 0. or abs(md.spot_price(time) / self.benchmark - 1) > self.threshold or is_eod:
                   net_delta = book.delta(time)
                   hedge_signal = Signal(Spot(md.spot_price(time), md.underlying), -1 * net_delta)
                   book.delta_hedge(hedge_signal, time)
                   self.benchmark = md.spot_price(time)
               return book
~~~

       

     - AutoCallRelativeGammaHedge(Strategy): 实现以gamma为基准的对冲

       - 具体实现：初始化的时候实现delta中性，并记录当前的gamma值为基准
       - 当现货价格偏离，导致账户delta变动超过*一定倍数（param）*的gamma之后，进行delta对冲，并更新基准gamma
       
       
~~~python
           def signal_run(self, book: Book, time, is_eod) -> Book:
               md = book.md()
             	#初始化的时候为delta中性，当账户的delta已经超过gamma的若干倍，进行对冲并更新基准gamma
               if abs(book.delta(time)) > abs(self.multiplier * self.benchmark_gamma) or is_eod:
                   hedge_signal = Signal(Spot(md.spot_price(time), md.underlying), -1 * book.delta(time))
                   book.delta_hedge(hedge_signal, time)
                   self.benchmark_gamma = book.gamma(time)
               return book
~~~

       

   - back_tester:

     - 回测框架的启动类
     - 分为几个主要步骤：
       - load数据，需要制定股票代码和起始日期，会根据交易日历load从起始日到结束日的所有市场数据
         - ***目前只支持起止日期都是交易日的，出于‘OO’考虑，会根据输入的起止日期自动寻找最近的交易日load数据***
       - 设定策略参数
       - 初始化book
       - 在book level run strategy并返回

     ~~~python
     data = MarketDataReader.load_market_data("600048", datetime(2021, 8, 2), datetime(2021, 8, 6))
     
     strategy1 = AutoCallStrategy(100000, '1d')
     
     book = Book.init_book(
         ProductLedger("", AutoCallInstrument("600048", data), 2)
     )
     result1 = BackTester(book).run(strategy1)
     
     ~~~

     

2. data

   1. trade_calendar.pkl 记录交易日历
   2. 在data/的文件夹下，每只股票会有自己的code对应的文件夹，文件夹下面有两个子文件夹，分别为spot(现货价格)和IT返回的产品数据(greeks/), 命名格式一致为： <股票代码>_<日期>.pkl

3. notebooks

   notebooks里分笔是对600048（保利地产）和他的autocall产品集中不同产品从2021-08-02到2021-08-05的回测结果



TODO:

1. 目前的启动类还可以封装，简化输入
2. 目前的数据返回格式为基本的市场数据和不同strategy对应的tag
3. 目前的启动类，是根据book的市场数据，在dataframe level用apply函数进行增量数据记录的，没有在每一个时间点做当前仓位和市场数据的快照，如果不能确保框架无误，在reconcile的时候可能会出现麻烦，在实现基本功能之后，需要增加unit-test模块，用dummy数据来保证存量数据和增量数据的一致性

