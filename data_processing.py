from optparse import OptionParser

import numpy as np
import os
import pickle
import pandas as pd
from datetime import datetime, timedelta
from utils.market_data_reader import load_trade_calendar


def extract_auto_call_greeks(code, trade_id, file_, data_dir_):
    df = pd.read_csv(data_dir_ + file_)
    df = df[df['trade_id'] == trade_id]
    df['bump_price'] = df['underlying_price'] * (1 + df['bump'])
    df = df.sort_values("bump_price")
    print(df)
    date_str = file_.split("_")[1].replace("-", "")
    df.to_pickle(f"./data/{code}/greeks/{code}_{date_str}.pkl")


def MOV(code, window=10, pcn=95):
    files = os.listdir(f"data/{code}/spot_new/")
    files.sort()
    # files = [x.split(".")[0].split("_")[1] for x in files]
    frame = pd.DataFrame()
    pre_close = None
    trade_days = load_trade_calendar(datetime(2020, 1, 6), datetime(2021, 12, 31))
    for date in files:
        df = pd.read_pickle(f"data/{code}/spot_new/{date}")
        df['is_eod'] = df.index == max(df.index)
        df['pre_close'] = pre_close
        pre_close = df[df['is_eod'] == True]['close'].values[0]
        df['mov'] = abs(df['close'] / df['pre_close'] - 1)
        frame = frame.append(df)
    frame['start_index'] = frame.index.strftime("%Y-%m-%d")
    # frame['start_index'] = frame['start_index'].apply(lambda x: datetime.strptime(x, "%Y-%m-%d"))

    """
    index_dates = {}
    for date in set(frame['start_index']):
        index_dates[date] = trade_days.index(date)
    """

    # frame['start_index'] = frame['start_index'].apply(lambda x: index_dates[x])
    '''
    frame['start'] = frame['start_index'].apply(lambda x: trade_days[x-window])
    frame['end'] = frame['start_index'].apply(lambda x: trade_days[x])
    frame['MOV_Signal'] = 0.
    '''
    frame = frame.dropna()

    for day in set(frame['start_index']):
        cut_frame = frame[frame['start_index'] == day]
        cut_frame.to_pickle(f"./data/{code}/spot_new/{code}_{day.replace('-', '')}.pkl")

    """cut_dict = frame[['start', 'end']].drop_duplicates().set_index('start')['end'].to_dict()
    mov_dict = {}

    for start, end in cut_dict.items():
        if start < datetime(2020, 10, 12):
            mov_dict[start] = None
        else:
            cut_frame = frame[(frame.index > start) & (frame.index < end)]
            mov_dict[start] = np.percentile(cut_frame['mov'].values, pcn)
    frame[f'{window}_day_{pcn}_mov'] = frame['start'].apply(lambda x: mov_dict[x])
    frame.to_pickle(f"./data/MOV_Signal/{window}_day_{pcn}_mov.pkl")
    return frame"""


if __name__ == "__main__":
    parser = OptionParser()
    parser.add_option("-s", dest="stock", action="store", type="string")
    (option, args) = parser.parse_args()
    stock = option.stock
    with open("./data/mapping.pkl", 'rb') as f:
        mapping = pickle.load(f)
    mapping = pickle.loads(mapping)
    data_dirs = ["./raw_data/lamp/", "./raw_data/lamp2/", "./raw_data/lamp3/"]
    for data_dir in data_dirs:
        files = os.listdir(data_dir)
        for file in files:
            if stock[0] == '0' or stock[0] == '3':
                code = stock + ".SZ"
            else:
                code = stock + ".SH"
            extract_auto_call_greeks(stock, int(mapping[code]), file, data_dir)

    # MOV("000998")
