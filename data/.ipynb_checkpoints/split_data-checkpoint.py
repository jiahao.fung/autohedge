import pandas as pd
import pickle
from datetime import datetime


with open("trade_calendar.pkl", 'rb')as f:
    trade_calendar = pickle.load(f)
trade_calendar = pickle.loads(trade_calendar)
kline_daily = pd.read_csv("002415_daily.csv", index_col = 0)


def get_pre_close(x):
    pre_index = trade_calendar[trade_calendar.index(datetime.strptime(x, "%Y-%m-%d")) - 1]
    return float(kline_daily.loc[pre_index.strftime("%Y-%m-%d")]['close'])


def calc_mov(x):
    high_mov = abs(x['high'] / x['pre_close'] - 1)
    low_mov = abs(x['low'] / x['pre_close'] - 1)
    return max(high_mov, low_mov)


def tear_candle_stock_into_days(df: pd.DataFrame, code):
    df['date_str'] = df.index.strftime("%Y-%m-%d")
    df['pre_close'] = df['date_str'].apply(lambda x: get_pre_close(x))
    dates_str = set(list(df['date_str']))
    df['mov'] = df.apply(lambda x: calc_mov(x), axis=1)
    for date_str in dates_str:
        sub_df = df[df['date_str'] == date_str]
        sub_df.to_pickle(f"./{code}/spot_new/{code}_{date_str.replace('-', '')}.pkl")


if __name__ == "__main__":
    df = pd.read_csv("002415.csv", index_col=0, parse_dates=True)
    # df.to_csv("002415.csv")
    # df['datetime'] = pd.to_datetime(df['datetime'])
    # df = df.set_index('datetime')
    tear_candle_stock_into_days(df, "002415")
