from datetime import datetime
from utils.ledger import *
from utils.signals import Signal


class Book:
    def __init__(self, ledger: ProductLedger) -> None:
        self.product_ledger = ledger
        self.hedge_ledger = HedgeLedger("")
        self.market_data = self.md()

    @classmethod
    def init_book(cls, arg: ProductLedger):
        assert isinstance(arg, ProductLedger), "Invalid input parameter: {}".format(arg)
        return cls(arg)

    def evaluate(self, time):
         return self.pv(time), self.delta(time), self.gamma(time), self.hedge_ledger.stock_position, self.pnl(time), self.hedge_ledger.avg_price

    def pnl(self, time):
        return self.hedge_ledger.pnl(self.market_data.spot_price(time))

    def pv(self, time) -> float:
        return self.product_ledger.pv(time) + self.hedge_ledger.pv(self.market_data.spot_price(time))

    def delta(self, time) -> float:
        return self.product_ledger.dollar_delta(time) + self.hedge_ledger.dollar_delta(self.market_data.spot_price(time))

    def gamma(self, time) -> float:
        return self.product_ledger.gamma(time) + self.hedge_ledger.gamma()

    def delta_hedge(self, signal: Signal, time):
        spot = self.market_data.spot_price(time)
        step_position = signal.qty / spot
        if self.hedge_ledger.stock_position + step_position != 0:
            self.hedge_ledger.avg_price = (self.hedge_ledger.stock_position * self.hedge_ledger.avg_price + signal.qty) / \
                                      (self.hedge_ledger.stock_position + step_position)
            self.hedge_ledger.position += signal.qty
            self.hedge_ledger.stock_position += step_position
        else:
            self.hedge_ledger.position += signal.qty
            self.hedge_ledger.stock_position = 0

    def md_frame(self):
        return self.product_ledger.product.md.market_data_series

    def md(self):
        return self.product_ledger.product.md
