from utils.instruments import Instrument


class Signal:

    def __init__(self, product: Instrument, qty) -> None:
        self.product = product
        self.qty = qty