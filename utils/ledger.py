import numpy as np
from utils.instruments import *


class Ledger(ABC):
    @abstractmethod
    def pv(self, *args):
        raise NotImplemented

    @abstractmethod
    def gamma(self, *args):
        raise NotImplemented

    @abstractmethod
    def dollar_delta(self, *args):
        raise NotImplemented


class ProductLedger(Ledger):
    def __init__(self, trade_id, instrument: AutoCallInstrument, position) -> None:
        self.code = "spot" # TODO:  fix the hard code one
        self.product = instrument
        self.position = position

    def pv(self, time):
        return self.product.pv(time) * self.position

    def gamma(self, time):
        return self.product.gamma(time) * self.position

    def dollar_delta(self, time):
        return self.product.dollar_delta(time) * self.position


class HedgeLedger(Ledger):
    def __init__(self, trade_id):
        self.trade_id = "spot"
        self.product = Spot(0., self.trade_id)
        self.position = 0.
        self.stock_position = 0.
        self.avg_price = 0.

    def pv(self, spot_price):
        return self.stock_position + spot_price

    def gamma(self):
        return 0.

    def dollar_delta(self, stock_price):
        return self.stock_position * stock_price

    def pnl(self, stock_price):
        if self.stock_position == 0:
            return np.nan
        else:
            return self.stock_position * (stock_price - self.avg_price)
    
