from abc import ABC, abstractmethod
from utils.book import Book


class Strategy(ABC):

    def tag_delta(self):
        return self.__str__() + " Delta"

    def tag_gamma(self):
        return self.__str__() + " Gamma"

    def tag_pv(self):
        return self.__str__() + " PV"

    def tag_hedge_position(self):
        return self.__str__() + " Delta Hedge Position"

    def tag_pnl(self):
        return self.__str__() + " P&L"

    def tag_signal(self):
        return self.__str__() + " Signal"

    def tag_hedge_pnl(self):
        return self.__str__() + " Hedge P&L"

    def tag_hedge_avg_price(self):
        return self.__str__() + " Avg Price"

    @abstractmethod
    def signal_run(self, *args) -> Book:
        raise NotImplemented
