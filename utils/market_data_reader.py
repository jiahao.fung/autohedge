import pandas as pd
import pickle
from datetime import datetime
from scipy.interpolate import interp1d


class MarketDataContainer:
    def __init__(self, underlying, market_data_series, open_close='close'):
        self.underlying = underlying
        self.open_close = open_close
        self.market_data_series = market_data_series

    def spot_price(self, time: datetime) -> float:
        return self.market_data_series.loc[time, self.open_close]

    def delta(self, time: datetime) -> float:
        return self.market_data_series.loc[time, 'delta']

    def gamma(self, time: datetime) -> float:
        return self.market_data_series.loc[time, 'gamma']

    def delta_pct(self, time: datetime) -> float:
        return self.market_data_series.loc[time, 'delta_pct']

    def pv(self, time: datetime) -> float:
        return self.market_data_series.loc[time, 'pv']

    def mov(self, time: datetime) -> float:
        return self.market_data_series.loc[time, 'mov']


class MarketDataReader:
    @staticmethod
    def readGreeks(code, date, file_dir):
        file = "%s/%s/greeks/%s_%s.pkl" % (file_dir, code, code, date.strftime("%Y%m%d"))
        # file = "%s/%s/greeks/%s.pkl" % (file_dir, code, code)
        return pd.read_pickle(file)

    @staticmethod
    def readTicker(code, date: datetime, file_dir):
        file = "%s/%s/spot_new/%s_%s.pkl" % (file_dir, code, code, date.strftime("%Y%m%d"))
        return pd.read_pickle(file)

    @staticmethod
    def load_data(code, date, file_dir, open_close):
        greeks = MarketDataReader.readGreeks(code, date, file_dir)
        ticker = MarketDataReader.readTicker(code, date, file_dir)
        ticker = ticker[[open_close, 'mov']]
        pv_interp = interp1d(x=greeks['bump_price'].values, y=greeks['pv'].values, fill_value="extrapolate")
        delta_interp = interp1d(x=greeks['bump_price'].values, y=greeks['delta'].values, fill_value="extrapolate")
        gamma_interp = interp1d(x=greeks['bump_price'].values, y=greeks['gamma'].values, fill_value="extrapolate")
        delta_pct_interp = interp1d(x=greeks['bump_price'].values, y=greeks['delta_pct'].values, fill_value="extrapolate")

        ticker['pv'] = ticker[open_close].apply(pv_interp).astype(float)
        ticker['delta'] = ticker[open_close].apply(delta_interp).astype(float)
        ticker['gamma'] = ticker[open_close].apply(gamma_interp).astype(float)
        ticker['delta_pct'] = ticker[open_close].apply(delta_pct_interp).astype(float)
        return ticker

    @staticmethod
    def load_market_data(code, start_datetime, end_datetime, file_dir='./data', open_close='close'):
        print("Start loading data")
        dates = load_trade_calendar(start_datetime, end_datetime)
        tickers = pd.DataFrame()
        for date in dates:
            # print(f"Loading data @ {date}")
            ticker = MarketDataReader.load_data(code, date, file_dir=file_dir, open_close=open_close)
            is_eod = ticker.index == max(ticker.index)
            ticker = ticker.assign(is_eod=is_eod)
            tickers = tickers.append(ticker)
        return MarketDataContainer(underlying=code, market_data_series=tickers, open_close='close')


def load_trade_calendar(start_datetime: datetime, end_datetime: datetime):
    with open("./data/trade_calendar.pkl", 'rb') as f:
        data = pickle.load(f)
    data = pickle.loads(data)
    rtn = data[data.index(start_datetime): data.index(end_datetime)]
    rtn.extend([end_datetime])
    return rtn


if __name__ == "__main__":
    # md = MarketDataReader.load_market_data("spot", datetime(2021, 8, 3), './data')
    # mkt_snap = MarketDataSnap().add_md("spot", md)
    dates = load_trade_calendar(datetime(2021, 8, 2), datetime(2021, 8, 10))
    print(dates)
