import math
from abc import ABC, abstractmethod
from scipy.stats import norm

from utils.market_data_reader import MarketDataContainer


class Instrument(ABC):

    @abstractmethod
    def delta(self, *args):
        raise NotImplemented

    @abstractmethod
    def gamma(self, *args):
        raise NotImplemented

    @abstractmethod
    def pv(self, *args):
        raise NotImplemented

    @abstractmethod
    def dollar_delta(self, *args):
        raise NotImplemented


class AutoCallInstrument(Instrument):

    def __init__(self, code, md: MarketDataContainer):
        self.code = code
        self.md = md

    def pv(self, time):
        return self.md.pv(time)

    def delta(self, time):
        return self.md.delta_pct(time)

    def dollar_delta(self, time):
        return self.md.delta(time)

    def gamma(self, time):
        return self.md.gamma(time)


class Spot(Instrument):

    def __init__(self, price, code) -> None:
        self.__price = price
        self.code = code

    def __str__(self) -> str:
        return "Spot-{}".format(self.pv)

    def delta(self):
        return 1.

    def dollar_delta(self):
        return self.__price

    def gamma(self):
        return 0

    def pv(self):
        return self.__price

    def setPrice(self, price):
        self.__price = price


class EuropeanOption(Instrument):
    TAG_CALL = "call"
    TAG_PUT = "put"

    def __init__(self, s, k, r, q, vol_pnt, t, payoff) -> None:
        self.spot = s
        self.strike = k
        self.r = r
        self.q = q
        self.vol = vol_pnt
        self.t = t
        assert payoff in [EuropeanOption.TAG_CALL, EuropeanOption.TAG_PUT], "invalid payoff type"
        self.payoff = payoff

    def __str__(self) -> str:
        return f"European {self.payoff} Option s->{self.spot} k->{self.strike} r->{self.r} q->{self.q} vol->{self.vol} tau->{self.t}"

    def pv(self) -> float:
        if self.payoff == EuropeanOption.TAG_CALL:
            return self.spot * math.exp(-self.q * self.t) * norm.cdf(self.d1()) - self.strike * math.exp(
                -self.r * self.t) * norm.cdf(self.d2())
        else:
            return self.strike * math.exp(-self.r * self.t) * norm.cdf(-self.d2()) - self.spot * math.exp(
                -self.q * self.t) * norm.cdf(-self.d1())

    def d1(self) -> float:
        return (math.log(self.spot / self.strike) + (self.r - self.q + 1 / 2 * self.vol ** 2) * self.t) / (
                    self.vol * math.sqrt(self.t))

    def d2(self) -> float:
        return self.d1() - math.sqrt(self.t) * self.vol

    def delta(self) -> float:
        if self.payoff == EuropeanOption.TAG_CALL:
            return math.exp(-1 * self.q * self.t) * norm.cdf(self.d1())
        else:
            return -1 * math.exp(-1 * self.q * self.t) * (1 - norm.cdf(self.d1()))

    def dollar_delta(self) -> float:
        return self.delta() * self.spot

    def gamma(self, *args) -> float:
        # TODO: calculate gamma
        return 0.

    def isExpire(self):
        return self.t == 0
