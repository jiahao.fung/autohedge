import logging
import datetime


LOG_FORMAT = "%(asctime)s - %(levelname)s - %(message)s"
logging.basicConfig(filename=f'./record/{datetime.datetime.now()}.log', level=logging.DEBUG, format=LOG_FORMAT)