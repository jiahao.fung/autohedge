from datetime import datetime
from optparse import OptionParser
from utils.market_data_reader import load_trade_calendar
import pandas as pd


def get_pre_close(x):
    return float(daily_frame.loc[get_pre_date(x)]['close'])


def get_pre_date(x: datetime):
    x = datetime.strptime(x, "%Y-%m-%d")
    return trade_calendar[trade_calendar.index(x) - 1]


def calc_mov(x):
    return max(abs(x['high'] / x['pre_close'] - 1), abs(x['low'] / x['pre_close'] - 1))


def tear_candle_stock_into_days(df: pd.DataFrame, code):
    df['date_str'] = df.index.strftime("%Y-%m-%d")
    df['pre_close'] = df["date_str"].apply(lambda x: get_pre_close(x))
    df['mov'] = df.apply(lambda x: calc_mov(x), axis=1)
    dates_str = set(list(df['date_str']))
    for date_str in dates_str:
        sub_df = df[df['date_str'] == date_str]
        sub_df.to_pickle(f"./data/{code}/spot_new/{code}_{date_str.replace('-', '')}.pkl")


if __name__ == "__main__":
    parser = OptionParser()
    parser.add_option("-s", dest="stock", action="store", type="string")
    (option, args) = parser.parse_args()
    stock = option.stock

    trade_calendar = load_trade_calendar(datetime(2020, 1, 6), datetime(2021, 9, 13))
    daily_frame = pd.read_csv(f"./data/{stock}_daily.csv", index_col=0, parse_dates=True)

    df = pd.read_csv(f"./data/{stock}.csv", index_col=0, parse_dates=True)
    # df.to_csv("600048.csv")
    # df['datetime'] = pd.to_datetime(df['datetime'])
    # df = df.set_index('datetime')
    tear_candle_stock_into_days(df, f"{stock}")
