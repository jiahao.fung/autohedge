import pandas as pd
from utils.logger_util import *
from utils.book import Book
from utils.instruments import Spot
from utils.signals import Signal
from utils.strategy import Strategy


class AutoCallStrategy(Strategy):

    def __init__(self, threshold, time_window):
        self.threshold = threshold
        self.time_window = time_window

    def __str__(self):
        return f"AUTO_CALL_HEDGE_{self.threshold}"

    def signal_run(self, book: Book, time, is_eod) -> Book:
        md = book.md()
        net_delta = book.delta(time)
        if is_eod or abs(net_delta) > self.threshold:
            # print(f"Signal for buying {-1 * net_delta} spot at time {time}")
            hedge_signal = Signal(Spot(md.spot_price(time), md.underlying), -1 * net_delta)
            book.delta_hedge(hedge_signal, time)
        return book


class AutoCallEODHedge(Strategy):
    def __init__(self):
        self.flag = True

    def __str__(self):
        return f"AUTO_CALL_EOD_HEDGE"

    def signal_run(self, book: Book, time, is_eod) -> Book:
        md = book.md()
        if is_eod or self.flag:
            if book.product_ledger.dollar_delta(time) < 0:
                hedge_signal = Signal(Spot(md.spot_price(time), md.underlying), -1 * book.delta(time))
                book.delta_hedge(hedge_signal, time)
            else:
                hedge_signal = Signal(Spot(md.spot_price(time), md.underlying),
                                     -1 * book.hedge_ledger.dollar_delta(md.spot_price(time)))
                book.delta_hedge(hedge_signal, time)
            if self.flag:
                self.flag = False
        return book


class AutoCallRelativeGammaHedge(Strategy):
    def __init__(self, gamma_multiplier, time_window):
        self.multiplier = gamma_multiplier
        self.time_window = time_window
        self.benchmark_gamma = 0.

    def __str__(self):
        return f"AUTO_CALL_RELATIVE_{self.multiplier}*GAMMA_HEDGE"

    def signal_run(self, book: Book, time, is_eod) -> Book:
        md = book.md()
        if abs(book.delta(time)) > abs(self.multiplier * self.benchmark_gamma) or is_eod:
            hedge_signal = Signal(Spot(md.spot_price(time), md.underlying), -1 * book.delta(time))
            book.delta_hedge(hedge_signal, time)
            self.benchmark_gamma = book.gamma(time)
        return book


class AutoCallRelativeHedgeStrategy(Strategy):
    def __init__(self, threshold, time_window):
        self.threshold = threshold
        self.time_window = time_window
        self.benchmark = 0.

    def __str__(self):
        return f"AUTO_CALL_RELATIVE_HEDGE_{100 * self.threshold}%"

    def signal_run(self, book: Book, time, is_eod) -> Book:
        md = book.md()
        if self.benchmark == 0. or abs(md.spot_price(time) / self.benchmark - 1) > self.threshold or is_eod:
            if not is_eod:
                logging.info(f"@Time: {time}, benchmark price: {self.benchmark}, spot price: {md.spot_price(time)}")
                logging.debug(f"Hedge {-1 * book.delta(time)} Delta")
            net_delta = book.delta(time)
            hedge_signal = Signal(Spot(md.spot_price(time), md.underlying), -1 * net_delta)
            book.delta_hedge(hedge_signal, time)
            self.benchmark = md.spot_price(time)
        return book


class MOVHedgeStrategy(Strategy):
    def __init__(self, window, threshold, code):
        self.signal_str = f"mov_signal_{window}_{threshold}"
        self.mov_signal_frame = pd.read_pickle(f"./data/{code}/mov_signal/{self.signal_str}.pkl")
        self.flag = True
        self.window = window
        self.threshold = threshold
        self.benchmark_price = 0.

    def __str__(self):
        return f"AUTO_CALL_MOV_HEDGE_{self.window}DAY_{int(self.threshold)}%"
    
    def mov_signal(self, time)->float:
        return float(self.mov_signal_frame[self.mov_signal_frame['date_str'] ==
                                     time.strftime("%Y-%m-%d")][self.signal_str])

    def signal_run(self, book: Book, time, is_eod) -> Book:
        md = book.md()
        spot = md.spot_price(time)
        if is_eod or self.flag or abs(spot / self.benchmark_price - 1) > self.mov_signal(time) :
            net_delta = book.delta(time)
            if book.product_ledger.dollar_delta(time) < 0:
                hedge_signal = Signal(Spot(spot, md.underlying), -1 * net_delta)
                book.delta_hedge(hedge_signal, time)
                self.benchmark_price = spot
            else:
                hedge_signal = Signal(Spot(spot, md.underlying), -1 *
                                      book.hedge_ledger.dollar_delta(spot))
                book.delta_hedge(hedge_signal, time)
            if self.flag:
                self.flag = False
        return book
    
class MOVGammaHedgeStrategy(Strategy):
    def __init__(self, window, percentile, code):
        self.signal_str = f"mov_signal_{window}_{percentile}"
        self.benchmark = pd.read_pickle(f"./data/{code}/mov_signal/{self.signal_str}.pkl")
        self.flag = True
        self.window = window
        self.percentile = percentile
        self.benchmark_gamma = 0.

    def __str__(self):
        return f"AUTO_CALL_MOV_HEDGE_{self.window}DAY_{int(self.percentile)}%"
    
    def mov_signal(self, time):
        return float(self.mov_signal_frame[self.mov_signal_frame['date_str']==time.strftime("%Y-%m-%d")][self.signal_str])

    def signal_run(self, book: Book, time, is_eod) -> Book:
        md = book.md()
        spot = md.spot_price(time)
        net_delta = book.delta(time)
        if is_eod or self.flag or md.mov(time) > self.mov_signal(time):
            hedge_signal = Signal(Spot(spot, md.underlying), -1 * net_delta)
            book.delta_hedge(hedge_signal, time)
            self.benchmark_price = spot
            
            if self.flag:
                self.flag = False
        return book
